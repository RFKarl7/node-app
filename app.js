// Import required modules
const express = require('express');

// Create an Express application
const app = express();

// Define a route
app.get('/', (req, res) => {
  res.send('Hello, Node.js!');
});

// Start the server
app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
